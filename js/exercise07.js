console.log(`\nExercise 07: Write the function golf_score_calculator wich accepts two strings and 
calculates the golf score of a game. Both strings will be of length 18, and 
each character in the string will be a number between 1 and 9 inclusive.`);

validate_string_number = (str) => {
  if (str.length != 18) {
    console.log("La longitud de la cadena debe ser de 18 digítos");
    return false;
  }
  for (let i = 0 ; i < str.length ; i++ ) {
    if (isNaN(str[i])) {
      console.log("Las entradas deben ser digítos númericos!");
      return false;
    }
  }
  return true;
}

golf_score_calculator = (s1, s2) => {
  let sum = 0;
  if (validate_string_number(s1) && validate_string_number(s2)) {
    for (let i = 0 ; i < s1.length ; i++ ) {
      let result = parseInt(s2[i], 0) - parseInt(s1[i], 0);
      console.log(`Hoyo ${(i+1)}: (${s2[i]} - ${s1[i]}) = ${result}`)
      sum += result;
    }
  }
  return sum;
}

let s1 = '473416723516543247';//score par
let s2 = '127959079874259913';//mi score

let final_score = golf_score_calculator(s1, s2);
console.log(`Mi score final es: ${final_score}`);