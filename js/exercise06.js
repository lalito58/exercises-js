console.log(`\nExercise 06: Implment the Luhn Algoritm wihch is used to help validate credit card numbers`);
//Función que realiza la suma de dos digitos...el cual resulta de multiplicar
//el digito par * 2, y si es mayor a 9
makeSum = (mynumber) => {
  let sum = 0;
  while ( mynumber > 0 ) {
    sum += mynumber % 10;
    mynumber = parseInt( (mynumber/10), 0);
  }
  return sum;//regresa la suma de los digitos
}
//Función principal que lleva a cabo el algoritmo de Luhn
luhnAlgorithm = (mynumber) => {
  console.log(`Main number: ${mynumber}`);
  let counter = 1;//contador para verificar si el digito es par
  let digit = 0;//el digito que se extrae de todo el numero
  let luhnArray = [];//El arreglo que contendrá el nuevo número
  let sum = 0;//la suma de los digitos del nuevo número
  while ( mynumber > 0 ) {//iremos partiendo el numero principal
    digit = mynumber % 10;
    mynumber = parseInt( (mynumber / 10), 0);
    if (counter == 2) {//si es el digito par
      digit = digit * 2;//se realiza la multiplicación por 2
      if (digit > 9) {//si el resultado es mayor a 9, entonces sumamos los digitos
        digit = makeSum(digit);//nos devuelve un solo digíto
      }
      counter = 1;//reseteamos contador
    } else {
      counter++;//sumamos 1 al contador
    }
    sum += digit;//haceos la suma principal
    luhnArray.push(digit);//y guardamos el digito al array
  }
  console.log(`Final number: ${luhnArray.reverse().join('')}`);
  console.log(`${sum} (modulus) 10 ===> ${(sum % 10)} and ...`);
  if ((sum % 10)  == 0 ) {
    console.log(`...is equal to 0, so this is a valid card number :).`);
  } else {
    console.log(`...is not equal to 0, so this is not a valid card number :(.`);
  }
}
let mynumber = 891;
luhnAlgorithm(mynumber);