console.log("\nExercise 02: Given an integer n, return the largest number that contains exactly n digits.");
largestNumber = (n) => {
  //n sería la potencia de 10, y el resultado le restariamos -1...
  //...para obtener el número mas largo.

  let result = 1;//inicia en porque 10^1 es 10...
  //Para no usar el pow (potencia de math -js-), haremos un ciclo con for
  for ( let i = 0 ; i < n ; i++ ) {
    result = result * 10;//10^1, 10^2, 10^3,...,10^n
  }

  //Usando pow (potencia de math -js-)
  /*let result = 0;
  result = Math.pow(10, n);*/
  
  //a eso le quitamos 1 para obtener el último número de n dígitos
  return result = result - 1;
}
//declaramos una variable con un número de dos digitos cualquiera
let n = 2;
//Invocamos la función
let result = largestNumber(n);
//Imprimimos el número mas largo para n
console.log("n = "+n+"\nlargestNumber(n) = "+result);