console.log("\nExercise 01: You are given a two-digit integer n. Return the sum of digits");
//Hoisting..primero declaramos la función para poder acceder a la misma :3
addTwoDigits = (n) => {
  let sum = 0;//variable que llevará la suma de los digitos obtenidos por mod
  while ( n > 0 ) {//ciclo while, que seguirá hasta que mi número n sea igual o menor a 0
    sum += n % 10;//Obtenemos un dígito del número principal, de derecha a izquierda, y sumamos
    n = parseInt( (n / 10), 0 );//obtenemos el nuevo número sin el dígito anterior...
  }
  return sum;
}
//declaramos una variable con un número de dos digitos cualquiera
let number = 59;
//Invocamos la función
let sum = addTwoDigits( number );
//Imprimimos la suma de los dígitos
console.log("n = "+number+"\naddTwoDigits(n) = " + sum );