console.log("\nExercise 04: Seats in Theater :)");
seatsInTheater = (nCols, nRows, col, row) => {
  for( let i = 0 ; i < nRows ; i++ ) {
    let div = document.createElement('div');
    for ( let j = 0 ; j < nCols ; j++ ){
      if ( i == row-1 && j == col-1) { 
        div.append(" (x) ");
      }else{
        div.append(" ( ) ");
      }
      
    }
    document.body.appendChild(div);
  }
  let newCols = (nCols - col) + 1;
  let newRows = nRows - row;
  return (newCols * newRows);
}

let nCols = 60;
let nRows = 100;
let col = 60;
let row = 1;

console.log("nCols = " + nCols + "\nnRows = " + nRows + "\ncol = " + col + "\nrow = " + row);

let allPeople = seatsInTheater(nCols, nRows, col, row);

console.log("seatsInTheater(nCols, nRows, col, row) = " + allPeople);