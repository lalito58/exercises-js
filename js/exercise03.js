console.log("\nExercise 03: n children have got m pieces of candy. They want to eat as much candy as they can, "+
          "but each child must eat exactly the same amount of candy as any other child. Determine how many pieces "+
          "of candy will be eaten by all the children together.");

let children = 3;
let candy = 10;
let res = candies( children, candy );

console.log("\ncandies(n, m) = "+parseFloat(res));

function candies( n, m ) {
  console.log("Hay " + n + " niños y " + m + " dulces, entonces: ");
  //obtenemos la parte entera de la división entre n y m
  let full = m/n;
  console.log("Parcialmente cada niño le tocará " + Math.trunc(full) +" dulces." );
  if( ( m - Math.trunc(full) * n )  > 0 ) {
    console.log("Pero en este caso sobraron " + ( m - Math.trunc(full) * n ) + " dulces. Lo que haremos es partir los dulces restantes entre los niños:");
    //So...
    let rest = ( m - Math.trunc(full) * n ) / n;
    console.log("Entonces...de los dulces restantes a cada niño le toca: " + rest.toFixed(2) + " dulces.");
    let allCandies = Math.trunc( full ) + parseFloat(rest.toFixed(2));
    console.log("Por lo tanto el total de dulces que cada chamaco le tocará son " + allCandies.toFixed(2));
    return ( parseFloat(allCandies.toFixed(2)) * n );
  }else{
    console.log("Viendo que la repartición fue exacta, cada niño le tocará " + full + " dulces.");
    return ( full * n );
  }

}