console.log(`\nExercise 08: Complete the function arrayManipulation in the editor below. 
It must return an integer, the maximum value in the resulting array.
arrayManipulation has the following parameters:
- n: the number of elements in your array, 
- queries - a two dimensional array of queries where each queries[i] contains three integers a, b, and k.\n\n`);

//función que valida las entradas del algoritmo
validateInputs = (nElem, mOper, array) => {
  if(nElem < 3 || nElem > Math.pow(10, 7)){
    console.log("Error (1)");
    return false;
  }
  if(mOper < 1 || mOper > (2 * Math.pow(10, 7))){
    console.log("Error (2)");
    return false;
  }
  for (let i = 0 ; i < array.length ; i++) {
    let a = array[i][0];
    let b = array[i][1];
    let k = array[i][2];
    if(a < 1 || (a > b) || (b > nElem)){
      console.log("Error (3)");
      return false;
    }
    if(k < 0 || k > Math.pow(10, 9)){
      console.log("Error (4)");
      return false;
    }
  }
  return true;
}
//Función que genera un arreglo de ceros
generateArrayZero = (nElem) => {
  let array = [];
  for (let i = 0 ; i <nElem ; i++) {
    array.push(0);
  }
  return array;//retorna el array de puros ceros
}
//Función que suma k a los elementos del array de acuerdo a los indices a y b
sumKByIndexesInArray = (array, a, b, k) => {
  for(let i = (a-1) ; i < b ; i++){
    array[i] = array[i] + k;
  }
  return array;
}
//función que de un arreglo obtiene el mayor valor
getMaxValue = (array) => {
  let maxValue = 0;
  for ( let i = 0 ; i < array.length ; i++ ) {
    if (maxValue <= array[i]){
      maxValue = array[i];
    }
  }
  return maxValue;
}
//Función principal, retorna el mayor valor del arreglo despues de m operaciones
arrayManipulation = (nElem, mOper, mat) => {
  let array = generateArrayZero(nElem);
  let maxValue = 0;
  console.log(`[${array}]`);
  for (let i = 0 ; i < mOper ; i++) {
    array = sumKByIndexesInArray(array, mat[i][0], mat[i][1], mat[i][2]);
    maxValue = getMaxValue(array);
    console.log(`[${array}]`);
  }
  return maxValue;
}
//creación de variables y el llamado a la función principal :3
let nElem = 5;
let mOper = 3;
let mat = [ [1, 2, 100], [2, 5, 100], [3, 4, 100] ];
let maxValue = 0;
if(validateInputs(nElem, mOper, mat)){
  maxValue = arrayManipulation(nElem, mOper, mat);
}

console.log(`Max Value = ${maxValue}`);
