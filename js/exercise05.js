console.log(`\nExercise 05: Count the number of occurrences of each character and
            return it as a list of tuples in order of appearance.`);

orderedCount = (str) => {
  //console.log("Tam " + str.length);
  let result = [];
  for (let i = 0 ; i < str.length ; i++) {
    if ( result.find( character => character[0] === str[i]) === undefined) {
      let countCharArray = [];
      countCharArray.push(str[i]);
      countCharArray.push(1);
      result.push(countCharArray);
    } else {
      let item = result.findIndex( character => character[0] === str[i]);
      result[item][1] = result[item][1] + 1;
    }
  }
  //console.log(result);
  printResult(result);
}

printResult = (result) => {
  for ( let i = 0 ; i < result.length ; i++ ) {
    console.log(result[i]);
  }
}

let myString = "el parangaricutirimicuaro se quiere desparangaricutirimicuarizar";
orderedCount(myString);