#Exercises-js

##Ejercicio 1:

You are given a two-digit integer n. Return the sum of digits.

####Example:
```javascript
n = 29
addTwoDigits(n) = 11 
```

##Ejercicio 2:

Given an integer n, return the largest number that contains exactly n digits.

####Example:
```javascript
n = 2
largestNumber(n) = 99
```

##Ejercicio 3:

n children have got m pieces of candy. They want to eat as much candy as they can, but each child must eat exactly the same amount of candy as any other child. Determine how many pieces of candy will be eaten by all the children together.

####Example:
```javascript
n = 3
m = 10
candies(n, m) = 9
```

##Ejercicio 4:

Seas in Theater :3

####Example:
```javascript
nCols = 60;
nRows = 100;
col = 60;
row = 1;
seatsInTheater(nCols, nRows, col, row) = 99
```

##Ejercicio 5:

Count the number of occurrences of each character and return it as a list of tuples in order of appearance.

####Example:
```javascript
orderedCount("abracadabra") = [ ["a", 5], ["b", 2], ["r", 2], ["c", 1], ["d", 1] ];
```

##Ejercicio 6:

Implement the Luhn Algorithm wich is used to help validate credit card numbers.

####Example:
```javascript
18 (modulus) 10 ===> 8, which is not equal to 0, so this is not a valid credit card number
```

##Ejercicio 7:

Write the function golf_score_calculator wich accepts two strings and 
calculates the golf score of a game. Both strings will be of length 18, and 
each character in the string will be a number between 1 and 9 inclusive.

####Example:
```javascript
golf_score_calculator('123456789876543210', '123456789876543210') = ?
```

##Ejercicio 8:

Complete the function arrayManipulation in the editor below. It must return an integer, the maximum value in the resulting array.
arrayManipulation has the following parameters:
- n: the number of elements in your array, 
- queries - a two dimensional array of queries where each queries[i] contains three integers a, b, and k.

###Input Format
The first line contains two space-separated integers n and m, the size of the array and the number of operations.
Each of the next m lines contains three space-separated integers a, b, and k, the left index, right index and summand.

###Constraints
3 <= n <= 10^7
1 <= m <= 2 * 10^5
1 <= a <= b <= n
0 <= k <= 10^9

####Example:
```javascript
//Sample input
n = 5 
m = 3
array = [
1 2 100
2 5 100
3 4 100
]

//Sample output
200

arrayManipulation(n, m, array) = 200
```